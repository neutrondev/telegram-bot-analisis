

(async () => {

    'use strict'

    // memanggil library telegram
    const telegram = require('telegram-bot-api')

    // menghubungkan library telegram dengan bot telegram menggunakan token
    // tokennya adalah 883123686:AAEkiUrA2GAN7StTkquyDDhtqbeTiOuGBcY
    // menambahkan paramater updates enabled true untuk mendapatkan update pesan ketika program pertama kali dijalankan
    const api = new telegram({ token: '883123686:AAEkiUrA2GAN7StTkquyDDhtqbeTiOuGBcY', updates: { enabled: true } })

    // memanggil library firebase 
    const admin = require('firebase-admin')

    // memanggil file kunci
    const serviceAccount = require("./pekbem-chabot-firebase-adminsdk-2d42y-c47ae10bb3.json")

    // menginisialisasi fiel kunci
    admin.initializeApp({ credential: admin.credential.cert(serviceAccount), databaseURL: "https://pekbem-chabot.firebaseio.com" })

    // memanggil library nlp.js
    const { NlpManager, ConversationContext } = require('node-nlp')

    // menginisialisasi bahasa id yang berarti bahasa indonesia
    const nlp = new NlpManager({ languages: ['id'], nlu: { useNoneFeature: false } })

    // membuat objek konteks
    const context = new ConversationContext()

    // menambahkan entitas
    nlp.addNamedEntityText(
        'calon',
        'Muhammad Afdal',
        ['id'],
        [
            'muhammad afdal',
            'Muh Afdal',
            'afdal',
            'nomor 1',
            'no 1',
            'no1'
        ]
    )

    nlp.addNamedEntityText(
        'calon',
        'Ahyar Abdullah',
        ['id'],
        [
            'ahyar abdullah',
            'ahyar',
            'rambo',
            'ahyarji',
            'nomor 2',
            'no 2',
            'no2'
        ]
    )

    nlp.addNamedEntityText(
        'calon',
        'Imam Maulana',
        ['id'],
        [
            'imam maulanan',
            'imam',
            'dodi',
            'omdod',
            'om dod',
            'nomor 3',
            'no 3',
            'no3'
        ]
    )

    // Menambahkan ucapan
    nlp.addDocument('id', 'saya pilih %calon%', 'pilih.calon')

    nlp.addDocument('id', '%calon% ku pilih', 'pilih.calon')

    nlp.addDocument('id', '%calon% saya', 'pilih.calon')

    nlp.addDocument('id', 'saya juga', 'pilih.calon')

    nlp.addDocument('id', '%calon% andalanku', 'pilih.calon')

    nlp.addDocument('id', 'bagus sebenarnya %calon% tapi lebih bagus %calon%', 'pilih.calon')

    nlp.addDocument('id', 'betul', 'pilih.calon')

    nlp.addDocument('id', '%calon% saya ku pilih', 'pilih.calon')

    nlp.addDocument('id', 'saya tidak pilih %calon% tapi %calon%', 'pilih.calon')

    nlp.addDocument('id', 'ganti deh saya pilih %calon%', 'pilih.calon')

    nlp.addDocument('id', 'tidak jadi deh pilih %calon% saya', 'pilih.calon')

    nlp.addDocument('id', 'saya tidak pilih %calon% tidak juga %calon% tapi %calon%', 'pilih.calon')

    nlp.addDocument('id', 'siapami ini kira" jadi ketu di"', 'tolak.calon')

    nlp.addDocument('id', 'saya tidak pilih %calon%', 'tolak.calon')

    nlp.addDocument('id', 'kenapa pilih %calon%', 'tolak.calon')

    nlp.addDocument('id', 'jangan pilih %calon%', 'tolak.calon')

    nlp.addDocument('id', 'janganmi pilih %calon%  %calon% mo saja atau %calon%', 'tolak.calon')

    nlp.addDocument('id', 'tidak mau pilih %calon%', 'tolak.calon')

    nlp.addDocument('id', 'bagus %calon% bagus juga %calon%', 'tolak.calon')

    nlp.addDocument('id', '%calon%', 'tolak.calon')

    nlp.addDocument('id', 'halo juga', 'tolak.calon')

    nlp.addDocument('id', 'hai juga', 'tolak.calon')

    nlp.addDocument('id', 'golput', 'tolak.calon')

    nlp.addDocument('id', 'Siapa yg jadi calon ketua bem ?', 'tolak.calon')

    nlp.addAnswer('id', 'pilih.calon', '{{calon}}')

    // menolak
    nlp.addAnswer('id', 'tolak.calon', 'Lainnya')

    // melatih nlp
    await nlp.train()

    // menyimpan hasil latihan
    nlp.save()

    const objToArr = obj => Object.keys(obj).map(i => {

        obj[i].key = i

        return obj[i]

    })

    api.on('message', async (message) => {

        console.log(message)

        const { text } = message

        const { username, first_name, last_name } = message.from

        const response = await nlp.process('id', text, context)

        const responseAnswer = response.answer ? response.answer : "Lainnya"

        const snap = await admin.database().ref("analisis").orderByChild('username').equalTo(username || first_name + " " + last_name).once("value")

        if (snap.exists() && response.answer && response.answer !== "Lainnya") {

            const snapshot = objToArr(snap.val())

            const result = snapshot.filter(i => new Date().getDate() === new Date(i.date).getDate() && i.intent !== 'Lainnya')

            const checkIntent = result.filter(i => i.intent !== response.answer)

            if (result.length < 1) {

                console.log("response : " + responseAnswer)

                admin.database().ref("analisis").push({
                    intent: response.answer ? response.answer : "Lainnya",
                    text,
                    date: admin.database.ServerValue.TIMESTAMP,
                    username: username || first_name + " " + last_name
                })

            } else {

                if (checkIntent.length > 0) {

                    console.log('ganti pilihan')

                    await admin.database().ref("analisis").child(checkIntent[0].key).update({
                        intent: response.answer ? response.answer : "Lainnya",
                        text,
                        date: admin.database.ServerValue.TIMESTAMP,
                        username: username || first_name + " " + last_name
                    })

                } else {

                    console.log('sudah menyatakan pilihan')
                }

            }

        } else {

            console.log(responseAnswer)

            admin.database().ref("analisis").push({
                intent: response.answer ? response.answer : "Lainnya",
                text,
                date: admin.database.ServerValue.TIMESTAMP,
                username: username || first_name + " " + last_name
            })
        }

    })

    console.log("bot running ...")

})()
