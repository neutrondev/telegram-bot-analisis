import React from 'react'

export default props => {

    return (
        <section className="hero-area">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">

                        <img
                            style={{ position: "absolute", top: "-159px", left: "-2px" }}
                            width="100"
                            src="https://pekbem-83ea2.web.app/images/logo.png"
                            alt=""
                        />

                        <div className="block">
                            <h1
                                className="wow fadeInUp"
                                data-wow-duration=".5s"
                                data-wow-delay=".3s"
                                style={{ fontSize: "40px" }}>
                                Selamat Datang
                                    <br /> Di Website Hasil Analisis <br /> dan Prediksi Calon Ketua BEM
                            </h1>
                            <p
                                className="wow fadeInUp"
                                data-wow-duration=".5s"
                                data-wow-delay=".5s">
                                Fakultas Ilmu Komputer Universitas Muslim Indonesia.
                            </p>
                            <ul className="list-inline wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".7s">

                                <li>
                                    <a data-scroll href="/hasil-analisis" className="btn btn-main">Lihat Hasil Analisis</a>
                                </li>
                                <li>
                                    <a data-scroll href="/hasil-prediksi" className="btn btn-transparent">Lihat Hasil Prediksi</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}