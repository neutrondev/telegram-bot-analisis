import React from 'react'
import ReactDOM from 'react-dom'
import firebase from "firebase/app"
import { BrowserRouter, Route, Switch } from "react-router-dom"

import Home from './home'
import HasilAnalisis from './hasil-analisis'
import HasilPrediksi from './hasil-prediksi'

import * as serviceWorker from './serviceWorker'

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDqKNIdaKgBtoZ-0z9mPHZ9fKXn3Guf8NY",
    authDomain: "pekbem-chabot.firebaseapp.com",
    databaseURL: "https://pekbem-chabot.firebaseio.com",
    projectId: "pekbem-chabot",
    storageBucket: "",
    messagingSenderId: "951386249126",
    appId: "1:951386249126:web:e2c4eb6fba4ba11c"
}

// Initialize Firebase
firebase.initializeApp(firebaseConfig)

ReactDOM.render(
    <BrowserRouter>
        <Switch>

            <Route exact path="/" render={props => <Home {...props} />} />

            <Route exact path="/hasil-analisis" render={props => <HasilAnalisis {...props} />} />

            <Route exact path="/hasil-prediksi" render={props => <HasilPrediksi {...props} />} />

        </Switch>
    </BrowserRouter>
    , document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
