import React, { useEffect } from 'react'
import firebase from "firebase/app"
import "firebase/database"
import Chart from "chart.js"
import dateFormat from "dateformat"

const objToArr = obj => Object.keys(obj).map(i => {

    obj[i].id_analisis = i

    obj[i].date = dateFormat(obj[i].date, "dd-mm-yyyy")

    return obj[i]

})

const chart = data => {

    const color = ['#b103fc', "#fc0331", '#037ffc', '#f5e342', '#b103fc', '#000']

    const element = document.getElementById("canvas")

    element.parentNode.removeChild(element);

    let ctx = document.createElement('canvas')

    ctx.setAttribute('id', 'canvas')

    document.getElementById("parent").appendChild(ctx)

    ctx = ctx.getContext("2d")

    data = Object.values(data).map((i, index) => {

        i.backgroundColor = color[index]

        i.borderColor = color[index]

        i.data = [i.data]

        return i

    })

    data = data.filter(i => i.label !== "Lainnya")

    new Chart(ctx, {

        // type: "horizontalBar",
        type: "bar",

        data: {
            labels: ["Hasil Prediksi Calon Ketua BEM"], // tanggal
            datasets: data
        },

        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }

    })

}

const prepare = value => {

    const newLabel = {}

    for (let i in value) {

        const intent = value[i].intent

        if (!newLabel[intent]) newLabel[intent] = {}

        newLabel[intent].label = intent

        newLabel[intent].data = (value.filter(i => i.intent === intent).length / value.length) * 100
    }

    return newLabel

}

export default props => {

    useEffect(() => {

        firebase.database().ref("analisis").on("value", snap => {

            const value = objToArr(snap.val())

            const data = prepare(value)

            chart(data)

        })

    }, [])

    return (
        <div className="App">
            {/* 
            <section className="single-page-header">
                <div className="container">
                <div className="row">
                    <div className="col-md-12">
                    <h2>Hasil Analisis</h2>
                    </div>
                </div>
                </div>
            </section> 
            */}

            <section className="buy-pro" style={{ padding: "30px 0" }}>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">

                            <a data-scroll href="/" className="btn btn-main">Kembali</a>

                            <center>
                                <h2>Hasil Prediksi</h2>
                            </center>

                        </div>
                    </div>
                </div>
            </section>

            <section className="buy-pro" style={{ padding: "20px 0 100px" }}>
                <div className="container">
                    <div className="row">

                        <div className="col-md-12">
                            <div id="parent" style={{ padding: "20px 30px", position: "relative", "background": "white", boxShadow: "0px 2px 28px 0px #7777775e", borderRadius: "5px" }}>
                                <canvas id="canvas" />
                            </div>
                        </div>

                    </div>
                </div>
            </section>

        </div>
    );
}
