import React, { useEffect, useState } from 'react'
import firebase from "firebase/app"
import "firebase/database"
import Chart from "chart.js"
import dateFormat from "dateformat"

const objToArr = obj => Object.keys(obj).map(i => {

  obj[i].id_analisis = i

  obj[i].date = dateFormat(obj[i].date, "dd-mm-yyyy")

  return obj[i]

})

const chart = data => {

  const color = ['#b103fc', "#fc0331", '#037ffc', '#f5e342', '#b103fc', '#000']

  const element = document.getElementById("canvas")

  element.parentNode.removeChild(element);

  let ctx = document.createElement('canvas')

  ctx.setAttribute('id', 'canvas')

  document.getElementById("parent").appendChild(ctx)

  ctx = ctx.getContext("2d")

  const label = Object.keys(data)

  let result = []

  let temporary = {}

  data = Object.values(data)

  for (let i in data) {

    const object = Object.values(data[i])

    for (let x in object) {

      if (object[x].name) {

        if (!temporary[object[x].name]) temporary[object[x].name] = {
          label: object[x].name,
          backgroundColor: color[x],
          borderColor: color[x],
          data: []
        }

        temporary[object[x].name].data[i] = object[x].persentase

        if (!temporary[object[x].name].data[i]) temporary[object[x].name].data[i] = 0

      }

    }
  }

  for (let i in Object.values(temporary)) result.push(Object.values(temporary)[i])


  label.push(dateFormat(new Date().setDate(new Date().getDate() + 1), "dd-mm-yyyy"))

  label.push(dateFormat(new Date().setDate(new Date().getDate() + 2), "dd-mm-yyyy"))

  label.push(dateFormat(new Date().setDate(new Date().getDate() + 3), "dd-mm-yyyy"))

  label.push(dateFormat(new Date().setDate(new Date().getDate() + 4), "dd-mm-yyyy"))

  label.push(dateFormat(new Date().setDate(new Date().getDate() + 5), "dd-mm-yyyy"))

  label.push(dateFormat(new Date().setDate(new Date().getDate() + 6), "dd-mm-yyyy"))

  new Chart(ctx, {

    // type: "horizontalBar",
    type: "bar",

    data: {
      labels: label, // tanggal
      datasets: result
    },


    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }

  })

}

const prepare = value => {

  const label = {}

  for (let i in value) {

    const date = value[i].date

    const intent = value[i].intent

    if (!label[date]) label[date] = {}

    label[date].totalData = value.filter(i => i.date === date).length

    if (!label[date][value[i].intent]) label[date][value[i].intent] = {}

    label[date][value[i].intent].persentase =
      (value.filter(i =>
        i.intent === intent && i.date === date).length / label[date].totalData) * 100

    label[date][value[i].intent].muncul =
      (value.filter(i => i.intent === intent && i.date === date).length)

    label[date][value[i].intent].name = value[i].intent
  }

  return label

}

export default props => {

  const [values, setValues] = useState([])

  useEffect(() => {

    firebase.database().ref("analisis").on("value", snap => {

      const value = objToArr(snap.val())

      const data = prepare(value)

      chart(data)

      setValues(data)

    })

  }, [])

  return (
    <div className="App">
      {/* 
      <section className="single-page-header">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h2>Hasil Analisis</h2>
            </div>
          </div>
        </div>
      </section> */}

      <section className="buy-pro" style={{ padding: "30px 0" }}>
        <div className="container">
          <div className="row">
            <div className="col-md-12">

              <a data-scroll href="/" className="btn btn-main">Kembali</a>

              <center>
                <h2>Hasil Analisis</h2>
              </center>

            </div>
          </div>
        </div>
      </section>

      <section className="buy-pro" style={{ padding: "20px 0 100px" }}>
        <div className="container">
          <div className="row">

            <div className="col-md-12">
              <div id="parent" style={{ padding: "20px 30px", position: "relative", "background": "white", boxShadow: "0px 2px 28px 0px #7777775e", borderRadius: "5px" }}>
                <canvas id="canvas" />
              </div>
            </div>

          </div>
        </div>
      </section>

      <section className="buy-pro" style={{ padding: "20px 0 100px" }}>
        <div className="container">
          <div className="row">

            <center>
              <h5>Data calon yang sering muncul</h5>
            </center>

            <div className="col-md-12">
              <div id="parent" style={{ padding: "20px 30px", position: "relative", "background": "white", boxShadow: "0px 2px 28px 0px #7777775e", borderRadius: "5px" }}>
                <table className="table">
                  <thead>
                    <tr>
                      <td rowSpan="2" style={{verticalAlign: "middle"}}>No</td>
                      <td rowSpan="2" style={{verticalAlign: "middle"}}>Nama</td>
                      <td
                        colSpan={Object.keys(values).length}
                        style={{ textAlign: "center" }}
                      >
                        Tanggal
                      </td>
                    </tr>
                    <tr>
                      {
                        Object.keys(values).map(i => {

                          return <td key={i} style={{ textAlign: "center" }}>{i}</td>
                        })
                      }
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Muhammad afdal</td>
                      {
                        Object.values(values).map((i, x) => {
                          return (
                            <td key={x} style={{ textAlign: "center" }}>
                              {i['Muhammad Afdal'] ? i['Muhammad Afdal'].muncul : '0'}
                            </td>
                          )
                        })
                      }
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Ahyar Abdullah</td>
                      {
                        Object.values(values).map((i, x) => {
                          return (
                            <td key={x} style={{ textAlign: "center" }}>
                              {i['Ahyar Abdullah'] ? i['Ahyar Abdullah'].muncul : '0'}
                            </td>
                          )
                        })
                      }
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Imam Maulana</td>
                      {
                        Object.values(values).map((i, x) => {
                          return (
                            <td key={x} style={{ textAlign: "center" }}>
                              {i['Imam Maulana'] ? i['Imam Maulana'].muncul : '0'}
                            </td>
                          )
                        })
                      }
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </section>

    </div>
  );
}
